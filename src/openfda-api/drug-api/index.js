const axios = require('axios')
const _ = require('lodash')
module.exports = class DrugApi {
    constructor(config) {
        this.config = _.clone(config)
        this.config.baseUrl = `${this.config.baseUrl}/drug`
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    async event() {
        let params = {limit:2}
        let url = `${this.baseUrl()}/event.json`
        return (await axios({
            method:'get',
            url,
            params:params,
            headers:this.headers()
        })).data    
    }
}
