const axios = require('axios')
const DrugApi = require('./drug-api')
module.exports = class OpenfdaApi {
    constructor() {
        this.config = {
            baseUrl: `https://api.fda.gov`,
            headers: {
                'X-Source': 'openfda-api',
                'Accept': 'application/json'
            }
        }
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }
    drug() {
        return new DrugApi(this.config)
    }
}