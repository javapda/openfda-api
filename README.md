# openfda-api #

## Description ##

* accessing FDA data via [openfda api](https://open.fda.gov)

## Resources ##

* [openFDA](https://open.fda.gov) | [api](https://open.fda.gov/apis) | [authentication](https://open.fda.gov/apis/authentication/)

## Glossary / Terminology ##

* [FDA - Food and Drug Administration](https://www.fda.gov/)
* [FAERS FDA Adverse Event Reporting System](https://open.fda.gov/data/faers/)
* SPL - Structured Product Labeling 

# disclaimer #
* from the openFDA site and its [Terms of Service](https://open.fda.gov/terms/)
```
Do not rely on openFDA to make decisions regarding medical care. Always speak to your health provider about the risks and benefits of FDA-regulated products. We may limit or otherwise restrict your access to the API in line with our Terms of Service
```

# credit #

* All credit is to [_Data provided by the U.S. Food and Drug Administration_](https://open.fda.gov)